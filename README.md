# Purchases Manage  
  
** Functionality: **  
  
+ crud operations with purchases  
+ reports on the database  
+ xml schema validation  
+ description rest-services with swagger  
+ basic auth for crud operations  
  
** Tools: **  
JDK 11, Spring Boot 2.1.6, Spring Data 5, Spring Security 5, JPA 2 / Hibernate 5, JAXB, jQuery 3, Twitter  Bootstrap 4, JUnit 4, Mockito 2, Springfox Swagger 2.9.2, Maven 3, Tomcat 8, PostgreSQL 11, IntelliJIDEA.  
  
** Notes: **  
SQL ddl and xml schema is located in the  `src/main/resources`  
  
_  
Гончаренко Виталий   
