package com.example.purchases.dao;

import com.example.purchases.model.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface PurchaseDao extends JpaRepository<Purchase, Integer> {

    @Query(nativeQuery = true, value = "select * from purchase p join (select a.id from (select a.purchaseitemid as id, " +
            "count(*) as count from purchaseinfo a where a.purchaseDate >= :date group by a.purchaseitemid) a where a.count " +
            "= (select max(count)from (select p.purchaseitemid as id, count(*) as count from purchaseinfo p where " +
            "p.purchaseDate >= :date group by purchaseitemid) a))a on p.id = a.id")
    List<Purchase> getMostPopularInMonth(@Param("date") LocalDate localDate);

    @Query(nativeQuery = true, value = "select * from purchase p join (select a.purchaseitemid from (select a.purchaseitemid, " +
            "count(*) as count from purchaseinfo a where a.age = 18 group by a.purchaseitemid) a where a.count = " +
            "(select max(a.count) from (select a.purchaseitemid, count(*) as count from purchaseinfo a where a.age = 18 " +
            "group by a.purchaseitemid) a)) a on a.purchaseitemid = p.id")
    List<Purchase> getMostPopularInAge(@Param("age") int age);
}
