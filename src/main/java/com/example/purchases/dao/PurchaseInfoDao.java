package com.example.purchases.dao;

import com.example.purchases.model.PurchaseInfo;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface PurchaseInfoDao extends JpaRepository<PurchaseInfo, Integer> {

    List<PurchaseInfo> getByPurchaseDateGreaterThanEqual(LocalDate localDate, Sort sort);

    @Query(nativeQuery = true, value = "select concat(a.name,' ', a.lastname) as name from (select p.name, p.lastName, " +
            "count(*) as count from purchaseinfo p where p.purchaseDate >= :date group by p.name, p.lastname) a where " +
            "a.count = (select max(a.count) from (select p.name, p.lastName, count(*) as count from purchaseinfo p where " +
            "p.purchaseDate >= :date group by p.name, p.lastname) a)")
    List<String> getMostPopularBuyer(@Param("date") LocalDate localDate);
}
