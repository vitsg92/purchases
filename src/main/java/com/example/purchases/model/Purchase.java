package com.example.purchases.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Objects;

@Entity
public class Purchase extends BaseEntity {

    @Enumerated(EnumType.STRING)
    private PurchaseName name;

    public Purchase() {
    }

    public Purchase(PurchaseName purchaseName) {
        this.name = purchaseName;
    }

    public PurchaseName getName() {
        return name;
    }

    public void setName(PurchaseName name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Purchase purchase = (Purchase) o;
        return name == purchase.name && getId() == purchase.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(name + String.valueOf(getId()));
    }
}
