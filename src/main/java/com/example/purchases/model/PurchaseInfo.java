package com.example.purchases.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseInfo extends BaseEntity {

    private String name;
    private String lastName;
    private int age;

    @ManyToOne
    @JoinColumn(name = "purchaseItemId")
    private Purchase purchaseItem;
    private int count;
    private double amount;

    @XmlJavaTypeAdapter(XmlLocalDateAdapter.class)
    private LocalDate purchaseDate;

    public PurchaseInfo() {
    }

    public PurchaseInfo(String name, String lastName, int age, Purchase purchaseItem, int count, double amount,
                        LocalDate purchaseDate) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.purchaseItem = purchaseItem;
        this.count = count;
        this.amount = amount;
        this.purchaseDate = purchaseDate;
    }

    public Purchase getPurchaseItem() {
        return purchaseItem;
    }

    public void setPurchaseItem(Purchase purchaseItem) {
        this.purchaseItem = purchaseItem;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFormattedDate() {
        return purchaseDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }
}
