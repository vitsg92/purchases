package com.example.purchases.model;

public enum PurchaseName {
    TV(49.5, "Телевизор"),
    SMARTPHONE(33.4, "Смартфон"),
    JUICE_EXTRACTOR(5.5, "Соковыжималка"),
    EARPHONES(3.9, "Наушники"),
    KEYBOARD(2.5, "Клавиатура");

    private double price;
    private String localName;

    PurchaseName(double price, String localName) {
        this.price = price;
        this.localName = localName;
    }

    public double getPrice() {
        return price;
    }

    public String getLocalName() {
        return localName;
    }
}
