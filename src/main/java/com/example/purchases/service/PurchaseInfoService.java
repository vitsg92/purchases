package com.example.purchases.service;

import com.example.purchases.dao.PurchaseInfoDao;
import com.example.purchases.model.PurchaseInfo;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Service
public class PurchaseInfoService {

    private final PurchaseInfoDao purchaseInfoDao;

    public PurchaseInfoService(PurchaseInfoDao purchaseInfoDao) {
        this.purchaseInfoDao = purchaseInfoDao;
    }

    @Transactional
    public PurchaseInfo savePurchaseInfo(PurchaseInfo purchaseInfo) {
        return purchaseInfoDao.save(purchaseInfo);
    }

    @Transactional
    public List<PurchaseInfo> getAllPurchasesInfo() {
        return purchaseInfoDao.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    @Transactional
    public PurchaseInfo getPurchaseInfo(int id) {
        return purchaseInfoDao.getOne(id);
    }

    @Transactional
    public void deletePurchaseInfo(int id) {
        purchaseInfoDao.deleteById(id);
    }

    @Transactional
    public List<PurchaseInfo> getWeekPurchases() {
        return purchaseInfoDao.getByPurchaseDateGreaterThanEqual(LocalDate.now().minusWeeks(1), Sort.by(Sort.Direction.DESC, "id"));
    }

    @Transactional
    public List<String> getMostPopularBuyer() {
        return purchaseInfoDao.getMostPopularBuyer(LocalDate.now().minusMonths(6));
    }
}
