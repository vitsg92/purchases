package com.example.purchases.service;

import com.example.purchases.dao.PurchaseDao;
import com.example.purchases.model.Purchase;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Service
public class PurchaseService {
    private static final int AGE = 18;

    private final PurchaseDao purchaseDao;

    public PurchaseService(PurchaseDao purchaseDao) {
        this.purchaseDao = purchaseDao;
    }

    @Transactional
    public List<Purchase> getAllPurchases() {
        return purchaseDao.findAll(Sort.by("id"));
    }

    @Transactional
    public List<Purchase> getMostPopularInMonth() {
        return purchaseDao.getMostPopularInMonth(LocalDate.now().minusMonths(1));
    }

    @Transactional
    public List<Purchase> getMostPopularInAge() {
        return purchaseDao.getMostPopularInAge(AGE);
    }
}
