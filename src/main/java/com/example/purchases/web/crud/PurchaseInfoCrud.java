package com.example.purchases.web.crud;

import com.example.purchases.model.PurchaseInfo;
import com.example.purchases.service.PurchaseInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

import static java.util.Objects.nonNull;

@Api
@RestController
public class PurchaseInfoCrud {

    private final PurchaseInfoService purchaseInfoService;

    public PurchaseInfoCrud(PurchaseInfoService purchaseInfoService) {
        this.purchaseInfoService = purchaseInfoService;
    }

    @ApiOperation(value = "Создание покупки")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = String.class),
            @ApiResponse(code = 400, message = "Bad request", response = String.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = String.class)
    })
    @RequestMapping(value = "/purchaseInfo", method = RequestMethod.PUT, consumes = MediaType.TEXT_XML_VALUE)
    public String createPurchaseInfo(@RequestBody PurchaseInfo purchaseInfo, HttpServletResponse response) {
        try {
            purchaseInfoService.savePurchaseInfo(purchaseInfo);
            return "Executed";
        } catch (RuntimeException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return "Internal Server Error";
        }
    }

    @ApiOperation(value = "Создание покупки", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = String.class),
            @ApiResponse(code = 400, message = "Bad request", response = String.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = String.class)
    })
    @RequestMapping(value = "/purchaseInfo", method = RequestMethod.POST, consumes = MediaType.TEXT_XML_VALUE)
    public String updatePurchaseInfo(@RequestBody PurchaseInfo purchaseInfo, HttpServletResponse response) {
        try {
            purchaseInfoService.savePurchaseInfo(purchaseInfo);
            return "Executed";
        } catch (RuntimeException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return "Internal Server Error";
        }
    }

    @ApiOperation(value = "Создание покупки", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = String.class),
            @ApiResponse(code = 400, message = "Bad request", response = String.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = String.class)
    })
    @RequestMapping(value = "/purchaseInfo/{id}", method = RequestMethod.DELETE)
    public String updatePurchaseInfo(@PathVariable int id, HttpServletResponse response) {
        try {
            purchaseInfoService.deletePurchaseInfo(id);
            return "Executed";
        } catch (RuntimeException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return "Internal Server Error";
        }
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handleXMLException(HttpMessageNotReadableException e) {
        Throwable rootCause = e.getRootCause();
        if (nonNull(rootCause)) {
            return "Error: " + rootCause.getMessage();
        } else {
            return "Error!";
        }
    }
}
