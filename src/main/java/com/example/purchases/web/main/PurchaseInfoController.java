package com.example.purchases.web.main;

import com.example.purchases.model.Purchase;
import com.example.purchases.service.PurchaseInfoService;
import com.example.purchases.service.PurchaseService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class PurchaseInfoController {

    private final PurchaseInfoService purchaseInfoService;
    private final PurchaseService purchaseService;

    public PurchaseInfoController(PurchaseInfoService purchaseInfoService, PurchaseService purchaseService) {
        this.purchaseInfoService = purchaseInfoService;
        this.purchaseService = purchaseService;
    }

    @RequestMapping(value = "/")
    public String getMainPage(Model model) {
        model.addAttribute("weekPurchasesInfo", purchaseInfoService.getWeekPurchases());
        return "mainPage";
    }

    @RequestMapping(value = "/purchasesInfo", method = RequestMethod.GET)
    public String getPurchasesInfo(Model model) {
        model.addAttribute("purchasesInfo", purchaseInfoService.getAllPurchasesInfo());
        model.addAttribute("purchases", purchaseService.getAllPurchases());
        return "purchasesInfo";
    }

    @RequestMapping(value = "/purchaseInfo/{id}", method = RequestMethod.GET)
    public String updatePurchaseInfo(@PathVariable int id, Model model) {
        model.addAttribute("updatedPurchaseInfo", purchaseInfoService.getPurchaseInfo(id));
        model.addAttribute("purchases", purchaseService.getAllPurchases());
        return "updatePurchaseInfo";
    }

    @ResponseBody
    @RequestMapping(value = "/mostPopularInMonth", method = RequestMethod.GET)
    public String getMostPopular(HttpServletResponse response) {
        try {
            StringBuilder sb = new StringBuilder();
            List<Purchase> purchases = purchaseService.getMostPopularInMonth();
            for (Purchase purchase : purchases) {
                sb.append(purchase.getName().getLocalName());
                sb.append("; ");
            }
            return sb.toString();
        } catch (RuntimeException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return "Internal Server Error";
        }
    }

    @ResponseBody
    @RequestMapping(value = "/mostPopularBuyer", method = RequestMethod.GET)
    public String getMostPopularBuyer(HttpServletResponse response) {
        try {
            StringBuilder sb = new StringBuilder();
            List<String> buyers = purchaseInfoService.getMostPopularBuyer();
            for (String buyer : buyers) {
                sb.append(buyer);
                sb.append("; ");
            }
            return sb.toString();
        } catch (RuntimeException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return "Internal Server Error";
        }
    }

    @ResponseBody
    @RequestMapping(value = "/mostPopularInAge", method = RequestMethod.GET)
    public String getMostPopularInAge(HttpServletResponse response) {
        try {
            StringBuilder sb = new StringBuilder();
            List<Purchase> purchases = purchaseService.getMostPopularInAge();
            for (Purchase purchase : purchases) {
                sb.append(purchase.getName().getLocalName());
                sb.append("; ");
            }
            return sb.toString();
        } catch (RuntimeException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return "Internal Server Error";
        }
    }
}
