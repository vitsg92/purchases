drop table if exists purchaseInfo;

drop table if exists purchase;

create table purchase (
    id  serial not null,
    name varchar(255),
    primary key (id));

create table purchaseInfo (
    id  serial not null,
    name varchar(255),
    lastName varchar(255),
    age int4 not null,
    purchaseItemId int4,
    count int4 not null,
    amount float8 not null,
    purchaseDate date,
    primary key (id));

alter table if exists purchaseInfo
    add constraint FKkf2scs9qm9uonp1us91dvx9id
    foreign key (purchaseItemId) references purchase;

insert into purchase(name) values('TV');
insert into purchase(name) values('SMARTPHONE');
insert into purchase(name) values('JUICE_EXTRACTOR');
insert into purchase(name) values('EARPHONES');
insert into purchase(name) values('KEYBOARD');

commit;