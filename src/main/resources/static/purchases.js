$(function () {
    var prefix = $('#prefix').val();
    $('#createPurchaseInfo').click(function () {
        $('#purchases').addClass('d-none');
        $('#createPurchaseInfoContainer').removeClass('d-none');
    });

    $('#formPurchase, #formCount').change(function () {
        $('#price').html((+$('#purchasePrice' + $('#formPurchase').val()).val() * +$('#formCount').val()).toFixed(2));
    });

    $('#purchaseInfoForm').on('submit', function () {
        var purchaseInfoXml = generateXml();
        $.ajax({
            url: prefix + '/purchaseInfo',
            data: purchaseInfoXml,
            method: 'put',
            contentType: 'text/xml'
        }).then(function () {
                location.href = prefix + '/purchasesInfo'
            },
            function (reason) {
                $('#error').html(
                    '<div class="alert alert-danger" role="alert">' +
                    '<strong>' + reason.responseText + '</strong>' +
                    '</div>'
                );
            });
        return false;
    });

    $('#updatePurchaseInfoForm').on('submit', function () {
        var purchaseInfoXml = generateXml();
        $.ajax({
            url: $('#prefix').val() + '/purchaseInfo',
            data: purchaseInfoXml,
            method: 'post',
            contentType: 'text/xml'
        }).then(function () {
                location.href = prefix + '/purchasesInfo'
            },
            function (reason) {
                $('#error').html(
                    '<div class="alert alert-danger" role="alert">' +
                    '<strong>' + reason.responseText + '</strong>' +
                    '</div>'
                );
            });
        return false;
    });

    $('.deletePurchaseInfo').on('click', function () {
        var button = $(this);
        $.ajax({
            url: $('#prefix').val() + $(this).attr('id'),
            method: 'delete'
        }).then(function (value) {
                button.replaceWith('<span class="btn btn-sm btn-danger disabled">Удален</span>');
            },
            function (reason) {
                $('#error').html(
                    '<div class="alert alert-danger" role="alert">' +
                    '<strong>Ошибка! </strong>' + reason.responseText +
                    '</div>'
                );
            });
        return false;
    });

    function generateXml() {
        var formPurchase = $('#formPurchase').val();
        return '<?xml version="1.0" encoding="UTF-8"?>' +
            '<purchaseInfo>' +
            '<id>' + $('#formId').val() + '</id>' +
            '<name>' + $('#formName').val() + '</name>' +
            '<lastName>' + $('#formLastName').val() + '</lastName>' +
            '<age>' + $('#formAge').val() + '</age>' +
            '<purchaseItem>' +
            '<id>' + $('#purchaseId' + formPurchase).val() + '</id>' +
            '<name>' + formPurchase + '</name>' +
            '</purchaseItem>' +
            '<count>' + $('#formCount').val() + '</count>' +
            '<amount>' + $('#price').html() + '</amount>' +
            '<purchaseDate>' + getCurrentDate() + '</purchaseDate>' +
            '</purchaseInfo>';
    }

    $('#mostPopularButton').click(function () {
        $.get(prefix + '/mostPopularInMonth').then(function (value) {
            var html = '<div><strong>Самый популярный товар за месяц: </strong></div><span>' + value + '</span>';
            var mainContainer = $('#mainContainer');
            mainContainer.empty();
            mainContainer.html(html);
        });
    });

    $('#mostPopularBuyer').click(function () {
        $.get(prefix + '/mostPopularBuyer').then(function (value) {
            var html = '<div><strong>Самый активный покупатель за пол года: </strong></div><span>' + value + '</span>';
            var mainContainer = $('#mainContainer');
            mainContainer.empty();
            mainContainer.html(html);
        });
    });

    $('#mostPopularOnAgeButton').click(function () {
        $.get(prefix + '/mostPopularInAge').then(function (value) {
            var html = '<div><strong>Самый популярный товар в 18 лет: </strong></div><span>' + value + '</span>';
            var mainContainer = $('#mainContainer');
            mainContainer.empty();
            mainContainer.html(html);
        });
    });

    function getCurrentDate() {
        var currentTime = new Date();
        var day = currentTime.getDate();
        if (day < 10) {
            day = '0' + day;
        }
        var month = currentTime.getMonth() + 1;
        if (month < 10) {
            month = '0' + month;
        }
        var year = currentTime.getFullYear();
        return day + '.' + month + '.' + year;
    }
});