<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Виталий Гончаренко
  Date: 16.07.2019
  Time: 0:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Main Page</title>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/purchases.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/purchases.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-8 offset-2 mt-5">
            <div class="d-flex justify-content-start mb-2">
                <a href="${pageContext.request.contextPath}/purchasesInfo" class="btn btn-sm btn-primary">Управление
                    покупками</a>
            </div>
            <div class="panel shadow-sm">
                <div class="row">
                    <div class="col-4">
                        <div class="btn-group-vertical mt-2 mb-2" role="group" aria-label="Basic example">
                            <a href="${pageContext.request.contextPath}/" class="btn btn-sm btn-success">Список покупок за неделю</a>
                            <button id="mostPopularButton" type="button" class="btn btn-sm btn-success">Самый популяный товар за месяц</button>
                            <button id="mostPopularBuyer" type="button" class="btn btn-sm btn-success">Самый активный покупатель за пол года
                            </button>
                            <button id="mostPopularOnAgeButton" type="button" class="btn btn-sm btn-success">Самый популярный товар в 18 лет
                            </button>
                        </div>
                    </div>
                    <div id="mainContainer" class="col-8">
                        <c:if test="${weekPurchasesInfo.size() == 0}">
                            <span>Покупок не найдено</span>
                        </c:if>
                        <c:if test="${weekPurchasesInfo.size() != 0}">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>Имя</th>
                                    <th>Покупка</th>
                                    <th>Количество</th>
                                    <th>Цена</th>
                                    <th>Дата</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="weekPurchaseInfo" items="${weekPurchasesInfo}">
                                    <tr>
                                        <td>${weekPurchaseInfo.name} ${weekPurchaseInfo.lastName}</td>
                                        <td>${weekPurchaseInfo.purchaseItem.name.localName}</td>
                                        <td>${weekPurchaseInfo.count}</td>
                                        <td>${weekPurchaseInfo.amount}</td>
                                        <td>${weekPurchaseInfo.getFormattedDate()}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input id="prefix" type="hidden" value="${pageContext.request.contextPath}">
</body>
</html>
