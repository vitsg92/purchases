<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Виталий Гончаренко
  Date: 16.07.2019
  Time: 15:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Purchases Info</title>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/purchases.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/purchases.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-8 offset-2 mt-5">
            <div id="error"></div>
            <div class="d-flex justify-content-between mb-2">
                <a href="${pageContext.request.contextPath}/" class="btn btn-sm btn-primary">Основная страница</a>
                <button id="createPurchaseInfo" class="btn btn-sm btn-primary">Произвести покупку</button>
            </div>
            <div class="panel shadow-sm">
                <div id="purchases">
                    <c:if test="${purchasesInfo.size() == 0}">
                        <span>Покупок не найдено</span>
                    </c:if>
                    <c:if test="${purchasesInfo.size() != 0}">
                        <div class="pt-1 pb-1 d-flex justify-content-center">
                            <h6>Найденные покупки</h6>
                        </div>
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Имя</th>
                                <th>Покупка</th>
                                <th>Количество</th>
                                <th>Цена</th>
                                <th>Дата</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${purchasesInfo}" var="purchaseInfo">

                                <tr>
                                    <td>${purchaseInfo.name} ${purchaseInfo.lastName}</td>
                                    <td>${purchaseInfo.purchaseItem.name.localName}</td>
                                    <td>${purchaseInfo.count}</td>
                                    <td>${purchaseInfo.amount}</td>
                                    <td>${purchaseInfo.getFormattedDate()}</td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/purchaseInfo/${purchaseInfo.id}" class="btn btn-sm btn-success">Редактировать</a>
                                    </td>
                                    <td>
                                        <button id="purchaseInfo/${purchaseInfo.id}" class="btn btn-sm btn-danger deletePurchaseInfo">Удалить</button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                </div>
                <div id="createPurchaseInfoContainer" class="d-none">
                    <form id="purchaseInfoForm">
                        <input type="hidden" id="formId" value="0">
                        <table>
                            <tr>
                                <td>Имя</td>
                                <td><input id="formName" type="text" name="name" class="form-control form-control-sm"
                                           required></td>
                                <td id="errorName"></td>
                            </tr>
                            <tr>
                                <td>Фамилия</td>
                                <td><input id="formLastName" type="text" name="lastName"
                                           class="form-control form-control-sm" required>
                                </td>
                                <td id="errorLastName"></td>
                            </tr>
                            <tr>
                                <td>Возраст</td>
                                <td><input id="formAge" type="text" name="age" class="form-control form-control-sm"
                                           required></td>
                                <td id="errorAge"></td>
                            </tr>
                            <tr>
                                <td>Покупка</td>
                                <td>
                                    <select id="formPurchase" name="purchaseItem" class="form-control form-control-sm"
                                            required>
                                        <option value="" selected disabled hidden>Выберите товар</option>
                                        <c:forEach var="purchase" items="${purchases}">
                                            <option value="${purchase.name}">${purchase.name.localName}</option>
                                            <option id="purchasePrice${purchase.name}" value="${purchase.name.price}"
                                                    hidden></option>
                                            <option id="purchaseId${purchase.name}" value="${purchase.id}"
                                                    hidden>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td id="errorPurchaseItem"></td>
                            </tr>
                            <tr>
                                <td>Количество</td>
                                <td>
                                    <select id="formCount" name="count" class="form-control form-control-sm" required>
                                        <option value="1" selected>1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </td>
                                <td id="errorCount"></td>
                            </tr>
                        </table>
                        <div>
                            <span>Итоговая сумма: </span>
                            <span id="price">0</span>
                        </div>
                        <button id="sendPurchaseItem" type="submit" class="btn btn-sm btn-primary mt-2">Сделать покупку
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<input id="prefix" type="hidden" value="${pageContext.request.contextPath}">
</body>
</html>
