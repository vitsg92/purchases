<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Виталий Гончаренко
  Date: 19.07.2019
  Time: 15:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update purchase info</title>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/purchases.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/purchases.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-8 offset-2 mt-5">
            <div id="error">

            </div>
            <div class="d-flex justify-content-between mb-2">
                <a href="${pageContext.request.contextPath}/" class="btn btn-sm btn-primary">Основная страница</a>
            </div>
            <div class="panel shadow-sm">
                <div id="updatePurchaseInfoContainer">
                    <form:form id="updatePurchaseInfoForm" modelAttribute="updatedPurchaseInfo">
                        <form:hidden id="formId" path="id"/>
                        <table>
                            <tr>
                                <td>Имя</td>
                                <td><form:input id="formName" type="text" path="name"
                                                class="form-control form-control-sm"
                                                required="required"/></td>
                                <td id="errorName"></td>
                            </tr>
                            <tr>
                                <td>Фамилия</td>
                                <td><form:input id="formLastName" type="text" path="lastName"
                                                class="form-control form-control-sm" required="required"/>
                                </td>
                                <td id="errorLastName"></td>
                            </tr>
                            <tr>
                                <td>Возраст</td>
                                <td><form:input id="formAge" type="text" path="age" class="form-control form-control-sm"
                                           required="required"/></td>
                                <td id="errorAge"></td>
                            </tr>
                            <tr>
                                <td>Покупка</td>
                                <td>
                                    <form:select id="formPurchase" path="purchaseItem" class="form-control form-control-sm"
                                            required="required">
                                        <option value="" selected disabled hidden>Выберите товар</option>
                                        <c:forEach var="purchase" items="${purchases}">
                                            <option value="${purchase.name}" <c:if test="${updatedPurchaseInfo.purchaseItem.name == purchase.name}">selected</c:if>>${purchase.name.localName}</option>
                                            <option id="purchasePrice${purchase.name}" value="${purchase.name.price}"
                                                    hidden></option>
                                            <option id="purchaseId${purchase.name}" value="${purchase.id}"
                                                    hidden>
                                            </option>
                                        </c:forEach>
                                    </form:select>
                                </td>
                                <td id="errorPurchaseItem"></td>
                            </tr>
                            <tr>
                                <td>Количество</td>
                                <td>
                                    <form:select id="formCount" path="count" class="form-control form-control-sm" required="required">
                                        <option value="1" <c:if test="${updatedPurchaseInfo.count == 1}">selected</c:if>>1</option>
                                        <option value="2" <c:if test="${updatedPurchaseInfo.count == 2}">selected</c:if>>2</option>
                                        <option value="3" <c:if test="${updatedPurchaseInfo.count == 3}">selected</c:if>>3</option>
                                        <option value="4" <c:if test="${updatedPurchaseInfo.count == 4}">selected</c:if>>4</option>
                                        <option value="5" <c:if test="${updatedPurchaseInfo.count == 5}">selected</c:if>>5</option>
                                    </form:select>
                                </td>
                                <td id="errorCount"></td>
                            </tr>
                        </table>
                        <div>
                            <span>Итоговая сумма: </span>
                            <span id="price">${updatedPurchaseInfo.amount}</span>
                        </div>
                        <button id="sendPurchaseItem" type="submit" class="btn btn-sm btn-primary mt-2">Изменить покупку
                        </button>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<input id="prefix" type="hidden" value="${pageContext.request.contextPath}">
</body>
</html>
