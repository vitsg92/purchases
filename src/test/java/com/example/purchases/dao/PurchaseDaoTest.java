package com.example.purchases.dao;

import com.example.purchases.model.PurchaseName;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

import static java.util.Objects.requireNonNull;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PurchaseDaoTest {

    @Autowired
    private PurchaseDao purchaseDao;

    @Autowired
    private DataSource dataSource;

    @Before
    public void onBefore() throws SQLException {
        Connection connection = dataSource.getConnection();
        RunScript.execute(connection, new InputStreamReader(requireNonNull(PurchaseDaoTest.class.getClassLoader().getResourceAsStream("data-h2-test.sql"))));
        connection.close();
    }

    @Test
    @Transactional
    public void testGetMostPopularInAge() {
        assertEquals(PurchaseName.SMARTPHONE, purchaseDao.getMostPopularInAge(18).get(0).getName());
    }

    @Test
    @Transactional
    public void testGetMostPopularInMonth() {
        assertEquals(PurchaseName.SMARTPHONE, purchaseDao.getMostPopularInMonth(LocalDate.of(2019, 7, 21).minusMonths(1))
                .get(0).getName());
    }
}
