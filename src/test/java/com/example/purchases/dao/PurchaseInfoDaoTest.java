package com.example.purchases.dao;

import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

import static java.util.Objects.requireNonNull;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PurchaseInfoDaoTest {

    @Autowired
    private PurchaseInfoDao purchaseInfoDao;

    @Autowired
    private DataSource dataSource;

    @Before
    public void onBefore() throws SQLException {
        Connection connection = dataSource.getConnection();
        RunScript.execute(connection, new InputStreamReader(requireNonNull(PurchaseDaoTest.class.getClassLoader().getResourceAsStream("data-h2-test.sql"))));
        connection.close();
    }

    @Test
    @Transactional
    public void testGetByPurchaseDateGreaterThanEqual() {
        assertEquals(18, purchaseInfoDao.getByPurchaseDateGreaterThanEqual(LocalDate.of(2019, 7, 21).minusWeeks(1),
                Sort.by(Sort.Direction.DESC, "id")).get(0).getAge());
        assertEquals(2, purchaseInfoDao.getByPurchaseDateGreaterThanEqual(LocalDate.of(2019, 7, 21).minusWeeks(1),
                Sort.by(Sort.Direction.DESC, "id")).get(0).getCount());
    }

    @Test
    @Transactional
    public void testGetMostPopularBuyer() {
        assertEquals("первый аккаунт", purchaseInfoDao.getMostPopularBuyer(LocalDate.of(2019, 7, 21).minusMonths(6))
                .get(0));
    }
}
