package com.example.purchases.service;

import com.example.purchases.dao.PurchaseInfoDao;
import com.example.purchases.model.Purchase;
import com.example.purchases.model.PurchaseInfo;
import com.example.purchases.model.PurchaseName;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PurchaseInfoServiceTest {
    @MockBean
    private PurchaseInfoDao purchaseInfoDao;
    @Autowired
    private PurchaseInfoService purchaseInfoService;
    private PurchaseInfo purchaseInfo = new PurchaseInfo("test", "test", 18, new Purchase(PurchaseName.TV), 3, 15.5,
            LocalDate.now());
    private PurchaseInfo purchaseInfoResult = new PurchaseInfo("test", "test", 18, new Purchase(PurchaseName.TV), 3, 15.5,
            LocalDate.now());

    @Before
    public void onBefore() {
        purchaseInfoResult.setId(1);
    }

    @Test
    public void testSavePurchaseInfo() {
        when(purchaseInfoDao.save(purchaseInfo)).thenReturn(purchaseInfoResult);
        assertEquals("test", purchaseInfoService.savePurchaseInfo(purchaseInfo).getLastName());
    }

    @Test
    public void testGetAllPurchasesInfo() {
        when(purchaseInfoDao.findAll(Sort.by(Sort.Direction.DESC, "id"))).thenReturn(
                Collections.singletonList(purchaseInfoResult));
        assertEquals(1, purchaseInfoService.getAllPurchasesInfo().size());
    }

    @Test
    public void testGetPurchaseInfo() {
        when(purchaseInfoDao.getOne(1)).thenReturn(purchaseInfoResult);
        assertEquals("test", purchaseInfoService.getPurchaseInfo(1).getName());
    }

    @Test
    public void testGetWeekPurchases() {
        when(purchaseInfoDao.getByPurchaseDateGreaterThanEqual(any(LocalDate.class), any(Sort.class)))
                .thenReturn(Collections.singletonList(purchaseInfoResult));
        assertEquals("test", purchaseInfoService.getWeekPurchases().get(0).getName());
    }

    @Test
    public void testGetMostPopularBuyer() {
        when(purchaseInfoDao.getMostPopularBuyer(any(LocalDate.class))).thenReturn(Collections.singletonList("test test"));
        assertEquals("test test", purchaseInfoService.getMostPopularBuyer().get(0));
    }
}