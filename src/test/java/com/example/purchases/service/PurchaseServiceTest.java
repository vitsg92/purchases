package com.example.purchases.service;

import com.example.purchases.dao.PurchaseDao;
import com.example.purchases.model.Purchase;
import com.example.purchases.model.PurchaseName;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PurchaseServiceTest {

    @MockBean
    private PurchaseDao purchaseDao;

    @Autowired
    private PurchaseService purchaseService;

    @Test
    public void testGetAllPurchases() {
        when(purchaseDao.findAll(Sort.by("id"))).thenReturn(Arrays.asList(new Purchase(PurchaseName.TV),
                new Purchase(PurchaseName.SMARTPHONE), new Purchase(PurchaseName.JUICE_EXTRACTOR), new Purchase(PurchaseName.EARPHONES),
                new Purchase(PurchaseName.KEYBOARD)));
        assertEquals(5, purchaseService.getAllPurchases().size());
    }

    @Test
    public void testGetMostPopularInMonth() {
        when(purchaseDao.getMostPopularInMonth(any(LocalDate.class))).thenReturn(Collections.singletonList(new Purchase(PurchaseName.TV)));
        assertEquals(PurchaseName.TV, purchaseService.getMostPopularInMonth().get(0).getName());
    }

    @Test
    public void testGetMostPopularInAge() {
        when(purchaseDao.getMostPopularInAge(any(Integer.class))).thenReturn(Collections.singletonList(new Purchase(PurchaseName.TV)));
        assertEquals(PurchaseName.TV, purchaseService.getMostPopularInAge().get(0).getName());
    }
}
